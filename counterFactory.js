
function counterFactory(i) {

    let currentValue = i;

    let increment = function (step) {

        currentValue += step;
        console.log(currentValue);
    };

    let decrement = function (step) {

        currentValue -= step;
        console.log(currentValue);
    }

    return {
        increment: increment,
        decrement: decrement
    };


}

module.exports = {
    counterFactory,
};


