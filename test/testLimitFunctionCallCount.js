let limitFunctionCallCount = require('../limitFunctionCallCount').limitFunctionCallCount;

const add = (a, b) => a + b;

const limitedAddFn = limitFunctionCallCount(add, 2);

console.log(limitedAddFn(1, 2)); // 3
console.log(limitedAddFn(3, 4)); // 7
console.log(limitedAddFn(4, 5)); // null


const cb = (x) => {
    return x * 2
};

let doublelimit = limitFunctionCallCount(cb, 3);

console.log(doublelimit(3));
console.log(doublelimit(6));
console.log(doublelimit(5));
console.log(doublelimit(10));