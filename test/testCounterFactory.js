let counterFactory = require('../counterFactory').counterFactory;

let Counter = counterFactory(0);


Counter.increment(1);
Counter.increment(1);
Counter.increment(1);
Counter.decrement(1);
Counter.decrement(1);